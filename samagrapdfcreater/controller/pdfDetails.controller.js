const dotenv = require("dotenv");
const resumeData = require("../PdfDataRetriver/candidateResume");
const pdfGenerator = require("../PdfDataRetriver/pdfCandicatesGenerator");
const pdfGenerators = require("../PdfDataRetriver/pdfMentorGenerator");
// const { log } = require("console");
dotenv.config("../.env");

const getpdfData = async (req, res) => {
  console.log(req.params.type, "uruuuuuuuuuuuuu");
  try {
    if (req.params.type == "mentor") {
      console.log("mentor");
      await pdfGenerators.pdfGenerators();
      res.status(200).send({ message: "mentor pdf template created" });
    } else if (req.params.type == "resume") {
      try {
      let data = await resumeData.resumeData();
        if (data.length != 0) {
          await pdfGenerator.pdfGenerator(data);
          res.status(200).send({ message: "resume pdf template created" });
        }
      } catch (err) {
        res.status(404).send("error" + err);
      }
    }
  } catch (err) {
    res.status(404).send("error" + err);
  }
};

module.exports = { getpdfData };
