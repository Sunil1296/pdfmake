
const resumeData = async ()=>{
    const fetch = require("node-fetch");
 const GRAPHQL_URL = "http://143.110.186.108:5001/v1/graphql";
  const AdminSecret = process.env.AdminSecret;
  let graphqlData =  await fetch(GRAPHQL_URL, {
    method: "POST",
    headers: {
        "content-type": "application/json",
        Accept: "application/json",
        "x-hasura-admin-secret": `${AdminSecret}`
      },
      body: JSON.stringify({
        query: `
        query MyQuery {
          candidate_profile (limit:10) {
            name
            mobile_number
            district_name {
              name
            }
            gender {
              gender_name
            }
            highest_level_qualification {
              id
              highest_level_qualification_name
            }
            current_employment {
              id
            }
            final_score_highest_qualification
            qualification_detail {
              qualification_name
              highest_level_qualification_id
            }
            job_role
            company_name
            work_experience_id
            skill_1
            skill_2
            skill_3
            skill_4
            english_knowledge_choice {
              english_choice
            }
            computer_operator {
              computer_operator_choice
            }
            driver_license {
              driver_license_choice
            }
          }
        }
        `,
      }),
 }) 
let data = await graphqlData.json();
 console.log(data,'9999999999999999999');
    return data.data
}
module.exports= {resumeData}