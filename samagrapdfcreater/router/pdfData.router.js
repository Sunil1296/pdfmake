const express = require("express");
const router = express.Router();
const pdfDetails = require("../controller/pdfDetails.controller")
const pdfResumeDetails = require("../controller/resume.controller")

module.exports = (app) => {
    router.get("/getpdfData/:type", pdfDetails.getpdfData);
    
    // router.get("/getpdfDatas", pdfResumeDetails.getpdfResumeData);
    // router.get("/getpdfData/:monitor", pdfDetails.getpdfResumeDataDetails);
    // router.get("/getpdfData/:resume", pdfDetails.getpdfResumeDataDetails);

    app.use("/get", router);
}