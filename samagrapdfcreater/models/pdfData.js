

const mongoose = require("mongoose");
const {userPdfConnection, } = require('./index');

const pdfDetails = new mongoose.Schema({
    content: Array,
    styles: Object,
    templatename:String
});

const userModel = userPdfConnection.model("pdfDatas",pdfDetails
);

 module.exports={
    userModel
 }