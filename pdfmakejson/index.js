var PdfPrinter = require("pdfmake");
var fs = require("fs");
var path = require("path")
const fonts = {
  Roboto: {
    normal: "fonts/Roboto-Regular.ttf",
    bold: "fonts/Roboto-Medium.ttf",
    italics: "fonts/Roboto-Italic.ttf",
    bolditalics: "fonts/Roboto-MediumItalic.ttf",
  },
};

// const content = require("./data.json");
const pdfContent=require("./pdfcontent.json")
// const content=require("./mentor")
// console.log(content);
  let docDefinition = {
    content: [
      {
                text: "School Visit Form\n\n",
                style: "header",
      },
      {  margin:[0,0,0,16],
              table: {
                widths: [510, '*', '*'],
                body: [
      [{
                  text: "Visit Overview",
                  style: "visit",
                  fillColor: "#EFEFEF",
                margin:[0,1,0,1]
      }]
      ]},
      layout:'noBorders',
      alignment:'center'
     
          
      },
      {
        style: 'tableExample',
        margin:[0,0,0,16],
        table: {
           widths: [504, '*', '*'],
          body: [
            [
              {
                stack: [
                  {  bold:true,
                  
                    ol: [{
                                     text: 'Form Filling Date:',margin:[0,2,0,2],bold:true
                                    
                    },{
                        text: 'Visit Date:',margin:[0,2,0,2],bold:true
                    },{
                           text: 'School:',margin:[0,2,0,2],bold:true
                    },
                    {
                           text: ' District:',margin:[0,2,0,2],bold:true
                    },
                    {
                           text: 'Block:',margin:[0,2,0,2],bold:true
                    },
                    {
                           text: 'Number of Grades Observed:',margin:[0,2,0,2],bold:true
                    },
                    {
                           text: 'Grades Observed:',margin:[0,2,0,2],bold:true
                    }
                    ],margin:[12,12,0,12]
                  }
                ]
              },
            ]
          ]
        }
        ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "black"
                    : "black";
                },
                vLineColor: function (i, node) {
                  return i === 0 || i === node.table.widths.length
                    ? "black"
                    : "black";
                },
              },
      },
        {  margin:[0,0,0,16],
              table: {
                widths: [510, '*', '*'],
                body: [
      [{
                  text: "Visit Details",
                  style: "visit",
                  fillColor: "#EFEFEF",
                margin:[0,1,0,1]
      }]
      ]},
      layout:'noBorders',
      alignment:'center'
     
          
      },
    
      {
        style: 'tableExample',
        table: {
          widths: [148, 110, 110, 110],
          heights:18,
          body: [
            [        {
                      text: "Question ",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                        decoration:'underline',
                        margin:[0,2,0,2]
                    },        {
                      text: "Class 1",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                        decoration:'underline',
                        margin:[0,2,0,2]
                    },        {
                      text: "Class 2",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                        decoration:'underline',
                        margin:[0,2,0,2]
                    },        {
                      text: "Class 3",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                        decoration:'underline',
                        margin:[0,2,0,2]
                    },],
          ]
        }
      },
      {
        style: 'tableExample',
        table: {
          widths: [505,0,0],
          heights:20,
          body: [
            [{text:'Basic Details', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
          ]
        }
              ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "white"
                    : "white";
                },}
      },
      {
        style: 'tableExample',
        table: {
            widths: [148, 110, 110, 110],
          body: [
            [{text:'Which class did you observe?',margin:[0,5,0,5]},{},{},{}],
            [{text:'Select the subject whose class you observed',margin:[0,5,0,5]},{},{},{}],
            [{text:'How many students are enrolled in this class?',margin:[0,5,0,5]},{},{},{}],
            [{text:'How many students of the class were present in school?',margin:[0,5,0,5]},{},{},{}],
          ]
        }
      },
      {
        style: 'tableExample',
        table: {
          widths: [505,0,0],
          heights:20,
          body: [
            [{text:'Class Observation', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
          ]
        }
              ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "white"
                    : "white";
                },}
      },
          {
        style: 'tableExample',
        table: {
           widths: [148, 110, 110, 110],
          body: [
            [{text:'Did the teacher prepare a lesson plan?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Which of the following sources did the teacher use to prepare the lesson plan?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Which source(s) of learning materials did the class use today?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Did the teacher use interactive teaching methods (games, activities, worksheets, etc.)?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Did the teacher give any',margin:[0,4,0,4]},{},{},{}],
            [{text:'written work to students today?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Does the teacher maintain a student wise record of competencies achieved?',margin:[0,4,0,4]},{},{},{}],
            [{text:'How many students had printed textbooks for the subject whose class you observed?',margin:[0,4,0,4]},{},{},{}],
          ]
        }
      },
          {
        style: 'tableExample',
        table: {
          widths: [505,0,0],
          heights:20,
          body: [
            [{text:'Students’ Assessment', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
          ]
        }
              ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "white"
                    : "white";
                },}
      },
      {
        style: 'tableExample',
        table: {
           widths: [148, 110, 110, 110],
          body: [
            [{text:'Out of the 5 randomly selectedstudents, how many answered the first question correctly?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Out of the 5 randomly selected students, how many answered the second question correctly?',margin:[0,4,0,4]},{},{},{}],
          ]
        }
      },
      {
        style: 'tableExample',
        table: {
          widths: [505,0,0],
          heights:20,
          body: [
            [{text:'Teacher Feedback', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
          ]
        }
              ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "white"
                    : "white";
                },}
      },
        {
        style: 'tableExample',
        table: {
           widths: [148, 110, 110, 110],
          body: [
            [{text:'Did you join the WhatsApp group of this class?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Did the teacher share the learning material taught in school on the class WhatsApp group?',margin:[0,4,0,4]},{},{},{}],
            [{text:'Did you share feedback with the teacher whose class you observed?',margin:[0,4,0,4]},{},{},{}],
            [{text:'What positive feedback did you share with the teacher?',margin:[0,4,0,4]},{},{},{}],
            [{text:'What improvement feedback did you share with the teacher?',margin:[0,4,0,4]},{},{},{}],
             ]
        }
      },
          {
        style: 'tableExample',
        table: {
          widths: [505,0,0],
          heights:20,
          body: [
            [{text:'Principal Interaction', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
          ]
        }
              ,layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "white"
                    : "white";
                },}
      },
        {
        style: 'tableExample',
        table: {
           widths: [148,348 ],
          body: [[{text:'In which of the following areas does the school need support?',margin:[0,4,0,4]},{}],
             ]
        }
      },
    ],
    styles: {
      header: {
                fontSize: 18,
                bold: true,
                alignment: "center",
                decoration:'underline',
                margin:[0,20,0,0]
              },
              visit:{
                fontSize: 12,
                bold: true,
                decoration:'underline',
              },
      subheader: {
        fontSize: 16,
        bold: true,
        margin: [0, 10, 0, 5]
      },
      tableExample: {
            fontSize: 11,
      },
      tableHeader: {
        bold: true,
        fontSize: 12,
        color: 'black'
      }
    },
  }
  console.log(docDefinition,'oooooooooo');
    var printer = new PdfPrinter(fonts);
  var pdfDoc = printer.createPdfKitDocument(docDefinition);
  pdfDoc.pipe(
    fs.createWriteStream('mentor.pdf')
  );
  pdfDoc.end();


// for (let i = 0; i < content.data.candidate_profile.length; i++) {
//   let docDefinition = {
//     content: [
//       { text: content.data.candidate_profile[i].name, style: "name" },
//       {
//         text: [
//           {
//             text: content.data.candidate_profile[i].whatsapp_mobile_number,
//             style: "mobile",
//           },
          
//         ],
//       },
//       {
//         text: "_______________________________________________________________________________________________\n\n",
//         style: "line",
//       },
//       {
//         ul: ["Employements Details"],
//         style: "name",
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Organisation Name : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].employer_organization_name,
//             style: "organisation",
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Company Name : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].company_name,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Current Company Name : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].company_name_current,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Job Roll : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].job_role,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Job Achievements : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].job_achievements,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Job Location : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].district_name.name,
//             style:"organisation"
//           },
//         ],
//       },
      
//       {
//         style: 'table',
//         text: [
//           { text: "Skill: ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].skill_1,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Expected Salary : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].expected_salary
//               .salary_range,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
            
//           { text: "employement Status : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].ever_employment
//               .employment_status,
//             style:"organisation",margin:[0,100,0,0]
//           },
//         ],
//       },
//       {
//         style: 'table',
//         ul: ["Personal Details"],
//         style: "name",
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Date Of Birth : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].DOB,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Gender : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].gender.gender_name,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Mobile NO : ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].mobile_number,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "PanCard: ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].pan_card.id,
//             style:"organisation"
//           },
//         ],
//       },
//       {
//         style: 'table',
//         text: [
//           { text: "Pincode: ", style: "main" },
//           {
//             text: content.data.candidate_profile[i].pincode,
//             style:"organisation"
//           },
//         ],
//       },
//     ],
//     styles: {
//       name: {
//         fontSize: 16,
//         bold: true,
//       },
//       table:{
//         margin: [19, 0, 0, 0],
//       },
//       main: {
//           margin:20,
//         fontSize: 12,

//         bold: true,
        
//       },
//       mobile: {
//         fontSize: 10,
//         color: "blue",
//       },
//       dob: {
//         alignment: "right",
//       },
//       organisation: {
//         fontSize: 12,
//         margin: [0, 40, 0, 0],
//       },
//     },
//   };

//   var printer = new PdfPrinter(fonts);
//   var pdfDoc = printer.createPdfKitDocument(docDefinition);
//   pdfDoc.pipe(
//     fs.createWriteStream(`${content.data.candidate_profile[i].name}.pdf`)
//   );
//   pdfDoc.end();

// for (let i = 0; i < pdfContent.pdfData.length; i++) {
//     let docDefinition = {
//       content: [{text:pdfContent.pdfData[i].name.text,style:"name"},
//     {text:pdfContent.pdfData[i].email,style:"email"},
//     {text:"_______________________________________________________________________________________________",style:"line"},{
//         ul:["software Engineer"],style:"main"
//     }
//     ],styles:{
//         name: {
//             fontSize: 18,
//             bold: true,
//           },
//           email: {
//             fontSize: 12,
//             decoration: "underline",
//             color: "blue",
//           },
//           main:{

//             fontSize: 18,
//             bold: true,
//             margin: 4,
//             // 		alignment:"center"
//           },
//       }
//     }

//       var printer = new PdfPrinter(fonts);
//       var pdfDoc = printer.createPdfKitDocument(docDefinition);
//       pdfDoc.pipe(
//         fs.createWriteStream(path.join(__dirname+`${content.data.candidate_profile[i].name}.pdf`))

//       );
//       pdfDoc.end();
// }
// }

