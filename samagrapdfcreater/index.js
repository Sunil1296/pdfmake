const express = require('express')
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const port = 5000;

require("./router/pdfData.router")(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
  });