module.exports={
      Nunito: {
          normal: 'fonts/Nunito-Light.ttf',
          bold: 'fonts/Nunito-Bold.ttf',
          italics: 'fonts/Nunito-LightItalic.ttf',
          bolditalics: 'fonts/Nunito-BoldItalic.ttf'
      },
      Spectral: {
          normal: 'fonts/Spectral-Regular.ttf',
          bold: 'fonts/Spectral-Bold.ttf',
          italics: 'fonts/Spectral-Italic.ttf',
          bolditalics: 'fonts/Spectral-BoldItalic.ttf'
      },Roboto: {
        normal: 'fonts/Roboto-Regular.ttf',
        bold: 'fonts/Roboto-Medium.ttf',
        italics: 'fonts/Roboto-Italic.ttf',
        bolditalics: 'fonts/Roboto-MediumItalic.ttf'
      }
     
   };

   