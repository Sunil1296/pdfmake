const pdfGenerators = async ()=>{
    var PdfPrinter = require("pdfmake");
    var fs = require("fs");
    const path = require("path");
    const fonts = {
        Roboto: {
          normal: "fonts/Roboto-Regular.ttf",
          bold: "fonts/Roboto-Medium.ttf",
          italics: "fonts/Roboto-Italic.ttf",
          bolditalics: "fonts/Roboto-MediumItalic.ttf",
        },
      };
      let docDefinition = {
        content: [
          {
                    text: "School Visit Form\n\n",
                    style: "header",
          },
          {  margin:[0,14,0,17],
                  table: {
                    widths: [510, '*', '*'],
                    body: [
          [{
                      text: "Visit Overview",
                      style: "visit",
                      fillColor: "#D9D9D9",
                    margin:[0,1,0,1]
          }]
          ]},
          layout:'noBorders',
          alignment:'center'    
          },
          {
            style: 'tableExample',
            margin:[0,0,0,17],
            table: {
               widths: [504, '*', '*'],
              body: [
                [
                  {
                    stack: [
                      {  bold:true,
                        fontSize: 10,
                        margin:[0,0,4,0],
                        ol: [{
                         text: 'Form Filling Date :',margin:[0,2,0,2],bold:true,fontSize: 10
                                        
                        },{
                         text: 'Visit Date :',margin:[0,2,0,2],bold:true,fontSize: 10
                        },{
                         text: 'School :',margin:[0,2,0,2],bold:true,fontSize: 10
                        },
                        {
                         text: 'District :',margin:[0,2,0,2],bold:true,fontSize: 10
                        },
                        {
                         text: 'Block :',margin:[0,2,0,2],bold:true,fontSize: 10
                        },
                        {
                         text: 'Number of Grades Observed :',margin:[0,2,0,2],bold:true,fontSize: 10
                        },
                        {
                         text: 'Grades Observed :',margin:[0,2,0,2],bold:true,fontSize: 10
                        }
                        ],margin:[14,10,0,10]
                      }
                    ]
                  },
                ]
              ]
            }
            ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "black"
                        : "black";
                    },
                    vLineColor: function (i, node) {
                      return i === 0 || i === node.table.widths.length
                        ? "black"
                        : "black";
                    },
                  },
          },
            {  margin:[0,4,0,16],
                  table: {
                    widths: [510, '*', '*'],
                    body: [
          [{
                      text: "Visit Details",
                      style: "visit",
                      fillColor: "#D9D9D9",
                      margin:[0,1,0,1]
          }]
          ]},
          layout:'noBorders',
          alignment:'center'   
          },
        
          {
            style: 'tableExample',
            table: {
              widths: [148, 110, 110, 110],
              heights:20,
              body: [
                [        {
                          text: "Question ",
                          style: "tableHeader",
                          alignment: 'center',
                          fillColor: "#CCCCCC",
                            decoration:'underline',
                            margin:[0,3,0,3]
                        },        {
                          text: "Class 1",
                          style: "tableHeader",
                          alignment: 'center',
                          fillColor: "#CCCCCC",
                            decoration:'underline',
                            margin:[0,3,0,3]
                        },        {
                          text: "Class 2",
                          style: "tableHeader",
                          alignment: 'center',
                          fillColor: "#CCCCCC",
                            decoration:'underline',
                            margin:[0,3,0,3]
                        },        {
                          text: "Class 3",
                          style: "tableHeader",
                          alignment: 'center',
                          fillColor: "#CCCCCC",
                            decoration:'underline',
                            margin:[0,3,0,3]
                        },],
              ]
            }
          },
          {
            style: 'tableExample',
            table: {
              widths: [505,0,0],
              heights:18,
              body: [
                [{text:'Basic Details', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 10}],
              ]
            }
                  ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "white"
                        : "white";
                    },}
          },
          {
            style: 'tableExample',
            table: {
                widths: [148, 110, 110, 110],
              body: [
                [{text:'Which class did you observe?',style:'tabledata',},{},{},{}],
                [{text:'Select the subject whose class you observed',style:'tabledata'},{},{},{}],
                [{text:'How many students are enrolled in this class?',style:'tabledata'},{},{},{}],
                [{text:'How many students of the class were present in school?',style:'tabledata'},{},{},{}],
              ]
            }
          },
          {
            style: 'tableExample',
            table: {
              widths: [505,0,0],
              heights:18,
              body: [
                [{text:'Class Observation', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 10}],
              ]
            }
                  ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "white"
                        : "white";
                    },}
          },
              {
            style: 'tableExample',
            table: {
               widths: [148, 110, 110, 110],
              body: [
                [{text:'Did the teacher prepare a lesson plan?',style:'tabledata'},{},{},{}],
                [{text:'Which of the following sources did the teacher use to prepare the lesson plan?',style:'tabledata'},{},{},{}],
                [{text:'Which source(s) of learning materials did the class use today?',style:'tabledata'},{},{},{}],
                [{text:'Did the teacher use interactive teaching methods (games, activities, worksheets, etc.)?',style:'tabledata'},{},{},{}],
                [{text:'Did the teacher give any written work to students today?',style:'tabledata'},{},{},{}],
                [{text:'Does the teacher maintain a student wise record of competencies achieved?',style:'tabledata'},{},{},{}],
                [{text:'How many students had printed textbooks for the subject whose class you observed?',style:'tabledata'},{},{},{}],
              ]
            }
          },
              {
            style: 'tableExample',
            table: {
              widths: [505,0,0],
              heights:20,
              body: [
                [{text:'Students’ Assessment', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 10}],
              ]
            }
                  ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "white"
                        : "white";
                    },}
          },
          {
            style: 'tableExample',
            table: {
               widths: [148, 110, 110, 110],
              body: [
                [{text:'Out of the 5 randomly selectedstudents, how many answered the first question correctly?',style:'tabledata'},{},{},{}],
                [{text:'Out of the 5 randomly selected students, how many answered the second question correctly?',style:'tabledata'},{},{},{}],
              ]
            }
          },
          {
            style: 'tableExample',
            table: {
              widths: [505,0,0],
              heights:20,
              body: [
                [{text:'Teacher Feedback', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 10}],
              ]
            }
                  ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "white"
                        : "white";
                    },}
          },
            {
            style: 'tableExample',
            table: {
               widths: [148, 110, 110, 110],
              body: [
                [{text:'Did you join the WhatsApp group of this class?',style:'tabledata'},{},{},{}],
                [{text:'Did the teacher share the learning material taught in school on the class WhatsApp group?',style:'tabledata'},{},{},{}],
                [{text:'Did you share feedback with the teacher whose class you observed?',style:'tabledata'},{},{},{}],
                [{text:'What positive feedback did you share with the teacher?',style:'tabledata'},{},{},{}],
                [{text:'What improvement feedback did you share with the teacher?',style:'tabledata'},{},{},{}],
                 ]
            }
          },
              {
            style: 'tableExample',
            table: {
              widths: [505,0,0],
              heights:20,
              body: [
                [{text:'Principal Interaction', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 10}],
              ]
            }
                  ,layout: {
                    hLineColor: function (i, node) {
                      return i === 0 || i === node.table.body.length
                        ? "white"
                        : "white";
                    },}
          },
            {
            style: 'tableExample',
            table: {
               widths: [148,348 ],
              body: [[{text:'In which of the following areas does the school need support?',style:'tabledata'},{}],
                 ]
            }
          },
        ],
        styles: {
          header: {
                    fontSize: 15,
                    bold: true,
                    alignment: "center",
                    decoration:'underline',
                  },
                  visit:{
                    fontSize: 10,
                    bold: true,
                    decoration:'underline',
                  },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
                fontSize: 11,
          },
          tableHeader: {
            bold: true,
            fontSize: 10,
            color: 'black'
          },tabledata:{
            margin:[0,5,0,5],
            // color:'#474747',
            fontSize: 10,
          }
        },
      }
      console.log(docDefinition,'oooooooooo');
        var printer = new PdfPrinter(fonts);
      var pdfDoc = printer.createPdfKitDocument(docDefinition);
      pdfDoc.pipe(
        fs.createWriteStream(
            path.join(__dirname + '../../pdf/mentor/mentor.pdf'))
            );
      pdfDoc.end();
}
module.exports= {pdfGenerators}