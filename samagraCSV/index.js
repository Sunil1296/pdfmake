const fs = require('fs')
const csv = require('csv-parser')
var PdfPrinter = require('pdfmake');
const styles = require('./styles')
const fonts = require('./font')

const proj = []
fs.createReadStream('demo.csv')
  .pipe(csv())

  .on('data', function (row) {
    let resume = {
      text: "Resume\n\n",
      style: 'header'
    }
    let name = {

      text: row.name,
      style: 'heading'
    }

    let email =
    {
      text: row.email,
      style: 'email'
    }
    let line = {
      text: "___________________________________________________________________________________"
    }

    let address = {
      text: row.address,
      style: 'address'

    }
    let gender = {
      text: row.gender,

    }
    let company = {

      text: row.company,

    }

    let Projects = [{
      text: "Projects",
      style: 'pheading'
    },

    {
      text: row.project1_name,
      style: 'projectname'
    }, {
      text: row.project1_description
    },
      , {
      ul: [
        [{ text: "FrontEnd", style: 'heading1' }, { text: row.project1_frontend }],
        [{ text: "Backend", style: 'heading1' }, { text: row.project1_backend }],
        [{ text: "Database", style: 'heading1' }, { text: row.project1_database }]
      ]
    },
      , {
      text: row.project2_name, style: 'projectname'
    }
      , {
      text: row.project2_description
    },
    {
      ul: [
        [{ text: "FrontEnd", style: 'heading1' }, { text: row.project2_frontend }],
        [{ text: "Backend", style: 'heading1' }, { text: row.project2_backend }],
        [{ text: "Database", style: 'heading1' }, { text: row.project2_database }]
      ]
    },
    ]
    let education = [{
      text: "Education",
      style: 'heading'
    }, {
      text: "BE", style: 'heading1'
    }
      , {
      text: row.Education_BE
    }, {
      text: "PUC", style: 'heading1'
    }, {
      text: row.Education_PUC
    }, {
      text: "SSLC", style: 'heading1'
    }, {
      text: row.Education_SSLC
    }

    ]
    let content = []
    content.push(resume, name, email, line, Projects, education)
    let docDefinition = {
      content: content,
      styles: styles
    }

    var printer = new PdfPrinter(fonts);
    var pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(fs.createWriteStream(`${row.name}.pdf`));
    pdfDoc.end();


  })
  