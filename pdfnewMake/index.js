const express = require('express')
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const port = 5000;
// const db=require("./models")
// db.mongoose
//   .connect(db.url, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//     useCreateIndex: true
//   })
//   .then(() => {
//     console.log("Connected to the database!");
//   })
//   .catch((err) => {
//     console.log("Cannot connect to the database!", err);
//     process.exit();
//   });

require("./router/pdfData.router")(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
  });