// module.exports = mongoose => {
//     const pdfData = mongoose.model(
//       "pdfData",
//       mongoose.Schema({})
//       );
  
//       return pdfData;
//     };

const mongoose = require("mongoose");
// const Schema = mongoose.Schema;
const {userPdfConnection, } = require('./index');

const pdfDetails = new mongoose.Schema({
    content: Array,
    styles: Object,
    templatename:String
});

const userModel = userPdfConnection.model("pdfDatas",pdfDetails
);

 module.exports={
    userModel
 }