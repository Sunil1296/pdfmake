module.exports= {
	content: [
    {
              text: "School Visit Form\n\n",
              style: "header",
    },
    {  margin:[0,0,0,16],
            table: {
              widths: [510, '*', '*'],
              body: [
    [{
                text: "Visit Overview",
                style: "visit",
                fillColor: "#EFEFEF",
              margin:[0,1,0,1]
    }]
    ]},
    layout:'noBorders',
    alignment:'center'
   
        
    },
		{
			style: 'tableExample',
			margin:[0,0,0,16],
			table: {
			   widths: [504, '*', '*'],
				body: [
					[
						{
							stack: [
								{  bold:true,
								
									ol: [{
                                   text: 'Form Filling Date:',margin:[0,2,0,2],bold:true
                                  
									},{
									    text: 'Visit Date:',margin:[0,2,0,2],bold:true
									},{
									       text: 'School:',margin:[0,2,0,2],bold:true
									},
									{
									       text: ' District:',margin:[0,2,0,2],bold:true
									},
									{
									       text: 'Block:',margin:[0,2,0,2],bold:true
									},
									{
									       text: 'Number of Grades Observed:',margin:[0,2,0,2],bold:true
									},
									{
									       text: 'Grades Observed:',margin:[0,2,0,2],bold:true
									}
									],margin:[12,12,0,12]
								}
							]
						},
					]
				]
			}
			,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "black"
                  : "black";
              },
              vLineColor: function (i, node) {
                return i === 0 || i === node.table.widths.length
                  ? "black"
                  : "black";
              },
            },
		},
		  {  margin:[0,0,0,16],
            table: {
              widths: [510, '*', '*'],
              body: [
    [{
                text: "Visit Details",
                style: "visit",
                fillColor: "#EFEFEF",
              margin:[0,1,0,1]
    }]
    ]},
    layout:'noBorders',
    alignment:'center'
   
        
    },
	
		{
			style: 'tableExample',
			table: {
				widths: [148, 110, 110, 110],
				heights:18,
				body: [
					[        {
                    text: "Question ",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                      decoration:'underline',
                      margin:[0,2,0,2]
                  },        {
                    text: "Class 1",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                      decoration:'underline',
                      margin:[0,2,0,2]
                  },        {
                    text: "Class 2",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                      decoration:'underline',
                      margin:[0,2,0,2]
                  },        {
                    text: "Class 3",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                      decoration:'underline',
                      margin:[0,2,0,2]
                  },],
				]
			}
		},
		{
			style: 'tableExample',
			table: {
				widths: [505,0,0],
				heights:20,
				body: [
					[{text:'Basic Details', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
				]
			}
						,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "white"
                  : "white";
              },}
		},
		{
			style: 'tableExample',
			table: {
					widths: [148, 110, 110, 110],
				body: [
					[{text:'Which class did you observe?',margin:[0,5,0,5]},{},{},{}],
					[{text:'Select the subject whose class you observed',margin:[0,5,0,5]},{},{},{}],
					[{text:'How many students are enrolled in this class?',margin:[0,5,0,5]},{},{},{}],
					[{text:'How many students of the class were present in school?',margin:[0,5,0,5]},{},{},{}],
				]
			}
		},
		{
			style: 'tableExample',
			table: {
				widths: [505,0,0],
				heights:20,
				body: [
					[{text:'Class Observation', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
				]
			}
						,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "white"
                  : "white";
              },}
		},
				{
			style: 'tableExample',
			table: {
			   widths: [148, 110, 110, 110],
				body: [
					[{text:'Did the teacher prepare a lesson plan?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Which of the following sources did the teacher use to prepare the lesson plan?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Which source(s) of learning materials did the class use today?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Did the teacher use interactive teaching methods (games, activities, worksheets, etc.)?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Did the teacher give any',margin:[0,4,0,4]},{},{},{}],
					[{text:'written work to students today?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Does the teacher maintain a student wise record of competencies achieved?',margin:[0,4,0,4]},{},{},{}],
					[{text:'How many students had printed textbooks for the subject whose class you observed?',margin:[0,4,0,4]},{},{},{}],
				]
			}
		},
				{
			style: 'tableExample',
			table: {
				widths: [505,0,0],
				heights:20,
				body: [
					[{text:'Students’ Assessment', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
				]
			}
						,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "white"
                  : "white";
              },}
		},
		{
			style: 'tableExample',
			table: {
			   widths: [148, 110, 110, 110],
				body: [
					[{text:'Out of the 5 randomly selectedstudents, how many answered the first question correctly?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Out of the 5 randomly selected students, how many answered the second question correctly?',margin:[0,4,0,4]},{},{},{}],
				]
			}
		},
		{
			style: 'tableExample',
			table: {
				widths: [505,0,0],
				heights:20,
				body: [
					[{text:'Teacher Feedback', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
				]
			}
						,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "white"
                  : "white";
              },}
		},
			{
			style: 'tableExample',
			table: {
			   widths: [148, 110, 110, 110],
				body: [
					[{text:'Did you join the WhatsApp group of this class?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Did the teacher share the learning material taught in school on the class WhatsApp group?',margin:[0,4,0,4]},{},{},{}],
					[{text:'Did you share feedback with the teacher whose class you observed?',margin:[0,4,0,4]},{},{},{}],
					[{text:'What positive feedback did you share with the teacher?',margin:[0,4,0,4]},{},{},{}],
					[{text:'What improvement feedback did you share with the teacher?',margin:[0,4,0,4]},{},{},{}],
					[{text:'',margin:[0,4,0,4]},{},{},{}],
					 ]
			}
		},
				{
			style: 'tableExample',
			table: {
				widths: [505,0,0],
				heights:20,
				body: [
					[{text:'Principal Interaction', alignment:'center',fillColor: "#EFEFEF",margin:[0,4,0,4],bold:true,fontSize: 13}],
				]
			}
						,layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "white"
                  : "white";
              },}
		},
			{
			style: 'tableExample',
			table: {
			   widths: [148,348 ],
				body: [[{text:'In which of the following areas does the school need support?',margin:[0,4,0,4]},{}],
					 ]
			}
		},
	],
	styles: {
	  header: {
              fontSize: 18,
              bold: true,
              alignment: "center",
              decoration:'underline',
              margin:[0,20,0,0]
            },
            visit:{
              fontSize: 12,
              bold: true,
              decoration:'underline',
            },
		subheader: {
			fontSize: 16,
			bold: true,
			margin: [0, 10, 0, 5]
		},
		tableExample: {
		    	fontSize: 11,
		},
		tableHeader: {
			bold: true,
			fontSize: 12,
			color: 'black'
		}
	},
	defaultStyle: {
		// alignment: 'justify'
	}
	
}
