
const {userModel} = require('../models/pdfData');
var PdfPrinter = require("pdfmake");
var fs = require("fs");
const path = require("path")
const fonts = {
  Roboto: {
    normal: "fonts/Raleway-Regular.ttf",
    bold: "fonts/Raleway-Bold.ttf",
    italics: "fonts/Raleway-Light.ttf",
    bolditalics: "fonts/Raleway-SemiBold.ttf",
  },
};

  const getpdfResumeData=(req,res)=>{
       let docDefinition ={
        content: [
          {
            text: "RESUME\n\n",
            style: "header",
          },
          {
            text:  '<Name>',
            style: "name",
          },
          {
            text: '+91-<Mobile Number>',
            style: "number",
          },
          {
            text:'<District, Haryana>',
            style: "number",
          },
          {
            text:'<Age>,  <Gender>\n\n',
            style: "gender",
          },
          {
            text: "EDUCATION",
            style: "main1",
          },
          {
            style: "tableExample",
            table: {
              widths: [150, 200, 150],
              heights: 22,
              body: [
                [
                  {
                    text: "Highest Qualification ",
                    style: "tableHeader",
                    fillColor: "#EFEFEF",
                    alignment: 'center'
                  },
                  {
                    text: "Specialisation",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                    decorationStyle: "dashed",
                  },
                  {
                    text: "Score",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                    decorationStyle: "dashed",
                  },
                ],
                [
                  {
                    text: 'Highest level qualification',
                    style: "tabledata ",
                    fontSize: 12,
                    alignment: 'center',
                    color:'#525252',
                    margin: [0, 4, 0, 4]
                  },
                  {
                    text: 'Qualification',
                    style: "tabledata",
                    alignment: 'center'
                  },
                  {
                    text:'<90%>',
                    style: "tabledata",
                    alignment: 'center'
                  },
                ],
              ],
            },
            layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "#EFEFEF"
                  : "#EFEFEF";
              },
              vLineColor: function (i, node) {
                return i === 0 || i === node.table.widths.length
                  ? "white"
                  : "white";
              },
            },
            alignment: "center",
            margin: [0, 0, 0, 10],
          },
          {
            text: "WORK EXPERIENCE",
            style: "main1",
          },
          {
            style: "tableExample",
            table: {
              widths: [150, 200, 150],
              heights: 22,
              margin:[0,3,0,0],
              body: [
                [
                  {
                    text: "Designation",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                  },
                  {
                    text: "Organization",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                  },
                  {
                    text: "Months of Experience",
                    style: "tableHeader",
                    alignment: 'center',
                    fillColor: "#EFEFEF",
                  },
                ],
                [
                  {
                    text: 'Job Role',
                    style: "tabledata",
                    alignment: 'center',
                  },
                  {
                    text: 'Name of company',
                    style: "tabledata",
                    alignment: 'center',
                  },
                  {
                    text: '<6> Months',
                    style: "tabledata",
                    alignment: 'center',
                  },
                ],
              ],
            },

            layout: {
              hLineColor: function (i, node) {
                return i === 0 || i === node.table.body.length
                  ? "#EFEFEF"
                  : "#EFEFEF";
              },
              vLineColor: function (i, node) {
                return i === 0 || i === node.table.widths.length
                  ? "white"
                  : "white";
              },
            },
            alignment: "center",
            margin: [0, 0, 0, 10],
          },
          {
            text: "SKILLS",
            style: "main",
          },
          {
            text: '<Skill 1>',
            style: "skills",
          },
          {
            text: '<Skill 2>',
            style: "skills",
          },
          {
            text: '<Skill 3>',
            style: "skills",
          },
          {
            text: '<Skill 4>',
            style: "skills",
          },
          {
            text: "PROFICIENCY",
            style: "main",
          },
          {
            style: "proeff",
            ul: [
              {
                text: [
                  { text: "English Speaking\t\t  :" },
                  {
                    text: '\t\t\t<Yes/No>',
                    margin: [30, 0, 0, 0],
                  },
                ],
              },
            ],
          },
          {
            style: "proeff1",
            ul: [
              {
                text: [
                  { text: "Computer Operation\t:" },
                  {
                    text: "\t\t\t<Yes/No>",
                    margin: [30, 0, 0, 0],
                  },
                ],
              },
            ],
          },
          {
            text: "AVAILABILITY OF DOCUMENTS",
            style: "subheader",
          },
          {
            style: "avail",
            ul: [
              {
                text: [
                  { text: "Driving License\t\t\t:" },
                  {
                    text: '\t\t\t<Yes/No>',
                    margin: [30, 0, 0, 0],
                  },
                ],
              },
            ],
          },
        ],
        styles: {
          header: {
            fontSize: 16,
            bold: true,
            alignment: "center",
            color: "#073763",
          },
          subheader: {
            fontSize: 16,
            bold: true,
            color: "#073763",
            margin: [0, 0, 0, 10],
          },
          name: {
            fontSize: 13,
            bold: true,
            color: "#073763",
            margin: [0, 0, 0, 10],
          },
          main: {
            fontSize: 16,
            bold: true,
            color: "#073763",
            margin: [0, 20, 0, 0],
          },
          main1: {
            fontSize: 16,
            bold: true,
            color: "#073763",
            margin: [0, 0, 0, 10],
          },
          skills: {
            margin: [10, 10, 0, 5],
            fontSize: 12,
            color:'#525252',
          },
          number: {
            fontSize: 12,
            color:'#525252',
          },
          gender: {
            color:'#525252',
            fontSize: 12,
            margin: [0, 0, 0, 10],
          },
          avail: {
            margin: [26, 10, 0, 10],
            fontSize: 12,
            color:'#525252',
          },
          proeff: {
            margin: [26, 10, 0, 10],
            fontSize: 12,
            color:'#525252',
          },
          proeff1: {
            margin: [26, 10, 0, 20],
            fontSize: 12,
            color:'#525252',
          },
          tableExample: {

          },
          tableHeader: {
            bold: true,
            color: "#073763",
            fontSize: 13,
            margin: [0, 4, 0, 4]
          },
          tabledata: {
            fontSize: 12,
            color:'#525252',
            margin: [0, 4, 0, 4]
          },
        },
      };
       console.log(docDefinition);
       var printer = new PdfPrinter(fonts);
       var pdfDoc = printer.createPdfKitDocument(docDefinition);
       pdfDoc.pipe(
         fs.createWriteStream(path.join(__dirname+'../../pdf/Resume.pdf'))
       );
       pdfDoc.end();
  }
  //
  const getpdfResumeDataDetails=(req,res)=>{
    console.log('hhhhhhhhhhhhhhhh');
  }

  module.exports={getpdfResumeData,getpdfResumeDataDetails}