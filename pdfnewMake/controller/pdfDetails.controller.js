// const { userModel } = require("../models/pdfData");
var PdfPrinter = require("pdfmake");
const fetch = require("node-fetch");
var fs = require("fs");
const path = require("path");
const dotenv = require("dotenv");
dotenv.config("../.env");
const fonts = {
  Roboto: {
    normal: "fonts/Raleway-Regular.ttf",
    bold: "fonts/Raleway-Bold.ttf",
    italics: "fonts/Raleway-Light.ttf",
    bolditalics: "fonts/Raleway-SemiBold.ttf",
  },
};
//
const getpdfData = (req, res) => {
  console.log(req.params,'gggggggggggggg');
  if(req.params==mentor){
  }else if(req.params==resume){
  }
  let candidatename = [];
  let mobile_number = [];
  let district_name = [];
  let gender = [];
  let highest_level_qualification = [];
  let current_employment = [];
  let final_score_highest_qualification = [];
  let qualification_detail = [];
  let job_role = [];
  let company_name = [];
  let work_experience_id = [];
  let skill_1 = [];
  let skill_2 = [];
  let skill_3 = [];
  let skill_4 = [];
  let english_knowledge_choice = [];
  let computer_operator = [];
  let driver_license = [];
  const GRAPHQL_URL = "http://143.110.186.108:5001/v1/graphql";
  const AdminSecret = process.env.AdminSecret;
  fetch(GRAPHQL_URL, {
    method: "POST",
    headers: {
      "content-type": "application/json",
      Accept: "application/json",
      "x-hasura-admin-secret": `${AdminSecret}`,
    },
    body: JSON.stringify({
      query: `
      query MyQuery {
        candidate_profile (limit:10) {
          name
          mobile_number
          district_name {
            name
          }
          gender {
            gender_name
          }
          highest_level_qualification {
            id
            highest_level_qualification_name
          }
          current_employment {
            id
          }
          final_score_highest_qualification
          qualification_detail {
            qualification_name
            highest_level_qualification_id
          }
          job_role
          company_name
          work_experience_id
          skill_1
          skill_2
          skill_3
          skill_4
          english_knowledge_choice {
            english_choice
          }
          computer_operator {
            computer_operator_choice
          }
          driver_license {
            driver_license_choice
          }
        }
      }
      `,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      data.data.candidate_profile.map((ele) => {
        candidatename.push(ele.name);
        mobile_number.push(ele.mobile_number);
        district_name.push(ele.district_name.name);
        gender.push(ele.gender.gender_name);
        highest_level_qualification.push(
          ele.highest_level_qualification.highest_level_qualification_name
        );
        current_employment.push(ele.current_employment.id);
        final_score_highest_qualification.push(
          ele.final_score_highest_qualification
        );
        qualification_detail.push(ele.qualification_detail.qualification_name);
        job_role.push(checkForData(ele.job_role));
        company_name.push(checkForData(ele.company_name));
        work_experience_id.push(checkForData(ele.work_experience_id));
        skill_1.push(checkForData(ele.skill_1));
        skill_2.push(checkForData(ele.skill_2));
        skill_3.push(checkForData(ele.skill_3));
        skill_4.push(checkForData(ele.skill_4));
        english_knowledge_choice.push(
          ele.english_knowledge_choice.english_choice
        );
        computer_operator.push(ele.computer_operator.computer_operator_choice);
        driver_license.push(ele.driver_license.driver_license_choice);
      });
      for (let i = 0; i < data.data.candidate_profile.length; i++) {
        let docDefinition = {
          content: [
            {
              text: "RESUME\n\n",
              style: "header",
            },
            {
              text: candidatename[i],
              style: "name",
            },
            {
              text: "+91-" + mobile_number[i],
              style: "number",
            },
            {
              text: district_name[i],
              style: "number",
            },
            {
              text: gender[i],
              style: "gender",
            },
            {
              text: "EDUCATION",
              style: "main1",
            },
            {
              style: "tableExample",
              table: {
                widths: [150, 200, 150],
                heights: 22,
                body: [
                  [
                    {
                      text: "Highest Qualification ",
                      style: "tableHeader",
                      fillColor: "#EFEFEF",
                      alignment: 'center'
                    },
                    {
                      text: "Specialisation",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                      decorationStyle: "dashed",
                    },
                    {
                      text: "Score",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                      decorationStyle: "dashed",
                    },
                  ],
                  [
                    {
                      text: highest_level_qualification[i],
                      style: "tabledata ",
                      fontSize: 12,
                      alignment: 'center',
                      color:'#525252',
                      margin: [0, 4, 0, 4]
                    },
                    {
                      text: qualification_detail[i],
                      style: "tabledata",
                      alignment: 'center'
                    },
                    {
                      text: final_score_highest_qualification[i],
                      style: "tabledata",
                      alignment: 'center'
                    },
                  ],
                ],
              },
              layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "#EFEFEF"
                    : "#EFEFEF";
                },
                vLineColor: function (i, node) {
                  return i === 0 || i === node.table.widths.length
                    ? "white"
                    : "white";
                },
              },
              alignment: "center",
              margin: [0, 0, 0, 10],
            },
            {
              text: "WORK EXPERIENCE",
              style: "main1",
            },
            {
              style: "tableExample",
              table: {
                widths: [150, 200, 150],
                heights: 22,
                margin:[0,3,0,0],
                body: [
                  [
                    {
                      text: "Designation",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                    },
                    {
                      text: "Organization",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                    },
                    {
                      text: "Months of Experience",
                      style: "tableHeader",
                      alignment: 'center',
                      fillColor: "#EFEFEF",
                    },
                  ],
                  [
                    {
                      text: job_role[i],
                      style: "tabledata",
                      alignment: 'center',
                    },
                    {
                      text: company_name[i],
                      style: "tabledata",
                      alignment: 'center',
                    },
                    {
                      text: work_experience_id[i],
                      style: "tabledata",
                      alignment: 'center',
                    },
                  ],
                ],
              },

              layout: {
                hLineColor: function (i, node) {
                  return i === 0 || i === node.table.body.length
                    ? "#EFEFEF"
                    : "#EFEFEF";
                },
                vLineColor: function (i, node) {
                  return i === 0 || i === node.table.widths.length
                    ? "white"
                    : "white";
                },
              },
              alignment: "center",
              margin: [0, 0, 0, 10],
            },
            {
              text: "SKILLS",
              style: "main",
            },
            {
              text: skill_1[i],
              style: "skills",
            },
            {
              text: skill_2[i],
              style: "skills",
            },
            {
              text: skill_3[i],
              style: "skills",
            },
            {
              text: skill_4[i],
              style: "skills",
            },
            {
              text: "PROFICIENCY",
              style: "main",
            },
            {
              style: "proeff",
              ul: [
                {
                  text: [
                    { text: "English Speaking\t\t  :" },
                    {
                      text: "\t\t\t" + english_knowledge_choice[i],
                      margin: [30, 0, 0, 0],
                    },
                  ],
                },
              ],
            },
            {
              style: "proeff1",
              ul: [
                {
                  text: [
                    { text: "Computer Operation\t:" },
                    {
                      text: "\t\t\t" + computer_operator[i],
                      margin: [30, 0, 0, 0],
                    },
                  ],
                },
              ],
            },
            {
              text: "AVAILABILITY OF DOCUMENTS",
              style: "subheader",
            },
            {
              style: "avail",
              ul: [
                {
                  text: [
                    { text: "Driving License\t\t\t:" },
                    {
                      text: "\t\t\t" + driver_license[i],
                      margin: [30, 0, 0, 0],
                    },
                  ],
                },
              ],
            },
          ],
          styles: {
            header: {
              fontSize: 16,
              bold: true,
              alignment: "center",
              color: "#073763",
            },
            subheader: {
              fontSize: 16,
              bold: true,
              color: "#073763",
              margin: [0, 0, 0, 10],
            },
            name: {
              fontSize: 13,
              bold: true,
              color: "#073763",
              margin: [0, 0, 0, 10],
            },
            main: {
              fontSize: 16,
              bold: true,
              color: "#073763",
              margin: [0, 20, 0, 0],
            },
            main1: {
              fontSize: 16,
              bold: true,
              color: "#073763",
              margin: [0, 0, 0, 10],
            },
            skills: {
              margin: [10, 10, 0, 5],
              fontSize: 12,
              color:'#525252',
            },
            number: {
              fontSize: 12,
              color:'#525252',
            },
            gender: {
              color:'#525252',
              fontSize: 12,
              margin: [0, 0, 0, 10],
            },
            avail: {
              margin: [26, 10, 0, 10],
              fontSize: 12,
              color:'#525252',
            },
            proeff: {
              margin: [26, 10, 0, 10],
              fontSize: 12,
              color:'#525252',
            },
            proeff1: {
              margin: [26, 10, 0, 20],
              fontSize: 12,
              color:'#525252',
            },
            tableExample: {

            },
            tableHeader: {
              bold: true,
              color: "#073763",
              fontSize: 13,
              margin: [0, 4, 0, 4]
            },
            tabledata: {
              fontSize: 12,
              color:'#525252',
              margin: [0, 4, 0, 4]
            },
          },
        };
        // console.log(docDefinition);
        var printer = new PdfPrinter(fonts);
        var pdfDoc = printer.createPdfKitDocument(docDefinition);
        pdfDoc.pipe(
          fs.createWriteStream(
            path.join(__dirname + `../../pdf/${candidatename[i]}.pdf`)
          )
        );
        pdfDoc.end();
      }
    });
};
//
const checkForData = (item) => {
  if (item == null || item == undefined) {
    return "NA";
  }
  return item;
};
//
const getpdfResumeDataDetails=(req,res)=>{
  // console.log(req,params,'dsdsdsads');
}

module.exports = { getpdfData,getpdfResumeDataDetails };
